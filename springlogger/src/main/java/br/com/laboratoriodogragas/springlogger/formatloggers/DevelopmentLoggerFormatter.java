package br.com.laboratoriodogragas.springlogger.formatloggers;

import java.text.SimpleDateFormat;
import java.time.Clock;
import java.util.Date;
import java.util.logging.Level;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.laboratoriodogragas.springlogger.dataclasses.LogRecord;
import br.com.laboratoriodogragas.springlogger.interfaces.FormatLogger;

public class DevelopmentLoggerFormatter implements FormatLogger {
  private ObjectMapper JSON;
  private Clock clock;

  public DevelopmentLoggerFormatter(Clock clock) {
    this.clock = clock;
    this.JSON = new ObjectMapper();
  }

  protected DevelopmentLoggerFormatter(Clock clock, ObjectMapper jsonMapper) {
    this.clock = clock;
    this.JSON = jsonMapper;
  }
  
  @Override
  public String format(Level logLevel, String callerClass, String callerMethod, Object logContent) {
    // [{Level} | {DateTime} | {Class} -> {Method}] - {Message} | Business Data: {BusinessData || none}
    String[] fullClassName = callerClass.split("\\.");
    String shortClassName = fullClassName[fullClassName.length - 1];
    String message = "";
    String businessData = "none";

    if (logContent instanceof LogRecord) {
      message = ((LogRecord) logContent).message();

      try {
        businessData = JSON.writeValueAsString(((LogRecord) logContent).businessData());
      } catch (Exception ignored) {
        // fuck
        businessData = "Tried to serialize, but throwed exception!";
      }
    } else {
      // Best effort logging
      message = String.valueOf(logContent);
    }

    String formattedMessage = "[" + logLevel.toString() + " | " +
      new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(clock.millis())) + " | " +
      shortClassName + " -> " +
      callerMethod + "] - " +
      message + " | " +
      "Business Data: " + businessData;

    if (logLevel.toString() == "SEVERE") {
      formattedMessage = "\u001B[31m" + formattedMessage + "\u001B[0m";
    }

    if (logLevel.toString() == "WARNING") {
      formattedMessage = "\u001B[33m" + formattedMessage + "\u001B[0m";
    }

    return formattedMessage;
  }
}
