package br.com.laboratoriodogragas.springlogger.formatloggers;

import java.text.SimpleDateFormat;
import java.time.Clock;
import java.util.Date;
import java.util.logging.Level;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.laboratoriodogragas.springlogger.dataclasses.LogRecord;
import br.com.laboratoriodogragas.springlogger.interfaces.FormatLogger;

public class KubernetesLoggerFormatter implements FormatLogger {
  private ObjectMapper JSON;
  private Clock clock;

  public KubernetesLoggerFormatter(Clock clock) {
    this.JSON = new ObjectMapper();
    this.clock = clock;
  }

  public KubernetesLoggerFormatter(Clock clock, ObjectMapper jsonMapper) {
    this.clock = clock;
    this.JSON = jsonMapper;
  }

  @Override
  public String format(Level logLevel, String callerClass, String callerMethod, Object logContent) {
    // {level: $level$, date: $date$, sourceClass: $class$, sourceMethod: $method$, businessData: $businessDataObj$}
    Object objMessage;

    if (logContent instanceof LogRecord) {
      LogRecord record = ((LogRecord) logContent);

      objMessage = new Object() {
        @JsonProperty
        String level = logLevel.toString();

        @JsonProperty
        String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(clock.millis()));

        @JsonProperty
        String sourceClass = callerClass;

        @JsonProperty
        String sourceMethod = callerMethod;

        @JsonProperty
        String message = record.message();

        @JsonProperty
        Object businessData = record.businessData();
      };
    } else {
      // Best effort logging
      objMessage = new Object() {
        @JsonProperty
        String level = logLevel.toString();

        @JsonProperty
        String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(clock.millis()));

        @JsonProperty
        String sourceClass = callerClass;

        @JsonProperty
        String sourceMethod = callerMethod;

        @JsonProperty
        String message = String.valueOf(logContent);

        @JsonProperty
        Object businessData = null;
      };
    }

    String messageToLog = "";

    try {
      messageToLog = JSON.writeValueAsString(objMessage);
    } catch (JsonProcessingException ignored) {
      messageToLog = "Failed to serialize businessData log object!";
    }

    return messageToLog;
  }
}
