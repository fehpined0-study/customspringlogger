package br.com.laboratoriodogragas.springlogger.dataclasses;

public record LogRecord(String message, BusinessData<?> businessData) {}
