package br.com.laboratoriodogragas.springlogger.dataclasses;

public record BusinessData<T>(T data) {}
