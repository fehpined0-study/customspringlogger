package br.com.laboratoriodogragas.springlogger;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class SimpleFormatter extends Formatter {

  @Override
  public String format(LogRecord record) {
    // This implementation formats the message on the Log implementation, so just log the entire message string;
    return record.getMessage() + '\n';
  }
}
