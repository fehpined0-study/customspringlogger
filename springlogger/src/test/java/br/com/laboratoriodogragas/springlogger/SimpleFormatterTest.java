package br.com.laboratoriodogragas.springlogger;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SimpleFormatterTest {
  @Test
  @DisplayName("Should return unformatted message with line break from record")
  void returnUnformattedMessage() {
    // ARRANGE
    SimpleFormatter formatter = new SimpleFormatter();
    LogRecord record = new LogRecord(Level.INFO, "Unformatted Message");

    // ACT
    String result = formatter.format(record);

    // ASSERT
    assertEquals(result, "Unformatted Message\n");
  }
}
