package br.com.laboratoriodogragas.springlogger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.juli.FileHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import br.com.laboratoriodogragas.springlogger.formatloggers.DevelopmentLoggerFormatter;
import br.com.laboratoriodogragas.springlogger.formatloggers.KubernetesLoggerFormatter;
import br.com.laboratoriodogragas.springlogger.interfaces.FormatLogger;

public class SpringLoggerTest {
  @Test
  @DisplayName("Constructor - Should instantiate with KubernetesLoggerFormatter if formatter option is json")
  void shouldInstantiateKubernetesFormatter() {
    // ARRANGE
    System.setProperty(SpringLogger.LOGGER_FORMATTER_OPTION_PROP, SpringLogger.LOG_FORMAT_JSON_TYPE);

    // ACT
    SpringLogger logger = new SpringLogger("any_name"); 

    // ASSERT
    assertInstanceOf(KubernetesLoggerFormatter.class, logger.getFormatter());
  }

  @Test
  @DisplayName("Constructor - Should instantiate with DevelopmentLoggerFormatter if formatter option is dev")
  void shouldInstantiateDevelopmentFormatter() {
    // ARRANGE
    System.setProperty(SpringLogger.LOGGER_FORMATTER_OPTION_PROP, SpringLogger.LOG_FORMAT_DEV_TYPE);
    
    // Just here to accomplish 100% coverage
    Logger.getLogger("").addHandler(mock(FileHandler.class));

    // ACT
    SpringLogger logger = new SpringLogger("any_name");

    // ASSERT
    assertInstanceOf(DevelopmentLoggerFormatter.class, logger.getFormatter());
  }

  @Test
  @DisplayName("isTraceEnabled - Should return true if logger is configured for FINEST level")
  void isTraceEnabledTrue() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.FINEST);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertTrue(logger.isTraceEnabled());
  }

  @Test
  @DisplayName("isTraceEnabled - Should return false if logger is configured for higher than FINEST level")
  void isTraceEnabledFalse() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.FINER);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertFalse(logger.isTraceEnabled());
  }

  @Test
  @DisplayName("isDebugEnabled - Should return true if logger is configured for FINER level")
  void isDebugEnabledTrue() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.FINER);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertTrue(logger.isDebugEnabled());
  }

  @Test
  @DisplayName("isDebugEnabled - Should return false if logger is configured for higher than FINER level")
  void isDebugEnabledFalse() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.FINE);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertFalse(logger.isDebugEnabled());
  }

  @Test
  @DisplayName("isInfoEnabled - Should return true if logger is configured for INFO level")
  void isInfoEnabledTrue() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.INFO);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertTrue(logger.isInfoEnabled());
  }

  @Test
  @DisplayName("isInfoEnabled - Should return false if logger is configured for higher than INFO level")
  void isInfoEnabledFalse() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.WARNING);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertFalse(logger.isInfoEnabled());
  }

  @Test
  @DisplayName("isWarnEnabled - Should return true if logger is configured for WARNING level")
  void isWarnEnabledTrue() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.WARNING);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertTrue(logger.isWarnEnabled());
  }

  @Test
  @DisplayName("isWarnEnabled - Should return false if logger is configured for higher than WARNING level")
  void isWarnEnabledFalse() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.SEVERE);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertFalse(logger.isWarnEnabled());
  }

  @Test
  @DisplayName("isErrorEnabled - Should return true if logger is configured for SEVERE level")
  void isErrorEnabledTrue() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.SEVERE);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertTrue(logger.isErrorEnabled());
  }

  @Test
  @DisplayName("isErrorEnabled - Should return false if logger is configured OFF level")
  void isErrorEnabledFalse() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.OFF);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertFalse(logger.isErrorEnabled());
  }

  @Test
  @DisplayName("isFatalEnabled - Should return true if logger is configured for SEVERE level")
  void isFatalEnabledTrue() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.SEVERE);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertTrue(logger.isFatalEnabled());
  }

  @Test
  @DisplayName("isFatalEnabled - Should return false if logger is configured OFF level")
  void isFatalEnabledFalse() {
    // ARRANGE
    Logger spLogger = Logger.getLogger(SpringLogger.class.getName());
    spLogger.setLevel(Level.OFF);

    // ACT
    SpringLogger logger = new SpringLogger();

    // ASSERT
    assertFalse(logger.isFatalEnabled());
  }

  @Test
  @DisplayName("trace - Should call formatter with correct parameters")
  void traceFormatArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setFormatter(mockFormatter);

    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    // ACT
    logger.trace(inputMessage);
    logger.trace(inputMessage, new IOException("hello"));

    // ASSERT
    verify(mockFormatter, atLeastOnce()).format(resultLevel.capture(), resultMessage.capture(), resultMessage.capture(), resultMessage.capture());
    assertEquals(resultMessage.getAllValues().get(0), "br.com.laboratoriodogragas.springlogger.SpringLoggerTest");
    assertEquals(resultMessage.getAllValues().get(1), "traceFormatArgs");
    assertEquals(resultMessage.getAllValues().get(2), inputMessage);
    assertEquals(Level.FINEST, resultLevel.getValue());
  }

  @Test
  @DisplayName("trace - Should call logger with correct parameters")
  void traceLoggingArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    Logger mockLogger = mock(Logger.class);
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setLogger(mockLogger).setFormatter(mockFormatter);
    
    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    when(mockFormatter.format(any(Level.class), anyString(), anyString(), anyString()))
      .thenReturn("Mocked Formatted Message");
    
    // ACT
    logger.trace(inputMessage);
    
    // ASSERT
    verify(mockLogger).log(resultLevel.capture(), resultMessage.capture());
    assertEquals(resultLevel.getValue(), Level.FINEST);
    assertEquals(resultMessage.getValue(), "Mocked Formatted Message");
  }

  @Test
  @DisplayName("debug - Should call formatter with correct parameters")
  void debugFormatArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setFormatter(mockFormatter);

    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    // ACT
    logger.debug(inputMessage);
    logger.debug(inputMessage, new IOException("hello"));

    // ASSERT
    verify(mockFormatter, atLeastOnce()).format(resultLevel.capture(), resultMessage.capture(), resultMessage.capture(), resultMessage.capture());
    assertEquals(resultMessage.getAllValues().get(0), "br.com.laboratoriodogragas.springlogger.SpringLoggerTest");
    assertEquals(resultMessage.getAllValues().get(1), "debugFormatArgs");
    assertEquals(resultMessage.getAllValues().get(2), inputMessage);
    assertEquals(Level.FINER, resultLevel.getValue());
  }

  @Test
  @DisplayName("debug - Should call logger with correct parameters")
  void debugLoggingArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    Logger mockLogger = mock(Logger.class);
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setLogger(mockLogger).setFormatter(mockFormatter);
    
    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    when(mockFormatter.format(any(Level.class), anyString(), anyString(), anyString()))
      .thenReturn("Mocked Formatted Message");
    
    // ACT
    logger.debug(inputMessage);
    
    // ASSERT
    verify(mockLogger).log(resultLevel.capture(), resultMessage.capture());
    assertEquals(resultLevel.getValue(), Level.FINER);
    assertEquals(resultMessage.getValue(), "Mocked Formatted Message");
  }

  @Test
  @DisplayName("debug - Should call formatter with correct parameters")
  void infoFormatArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setFormatter(mockFormatter);

    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    // ACT
    logger.info(inputMessage);
    logger.info(inputMessage, new IOException("hello"));

    // ASSERT
    verify(mockFormatter, atLeastOnce()).format(resultLevel.capture(), resultMessage.capture(), resultMessage.capture(), resultMessage.capture());
    assertEquals(resultMessage.getAllValues().get(0), "br.com.laboratoriodogragas.springlogger.SpringLoggerTest");
    assertEquals(resultMessage.getAllValues().get(1), "infoFormatArgs");
    assertEquals(resultMessage.getAllValues().get(2), inputMessage);
    assertEquals(Level.INFO, resultLevel.getValue());
  }

  @Test
  @DisplayName("info - Should call logger with correct parameters")
  void infoLoggingArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    Logger mockLogger = mock(Logger.class);
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setLogger(mockLogger).setFormatter(mockFormatter);
    
    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    when(mockFormatter.format(any(Level.class), anyString(), anyString(), anyString()))
      .thenReturn("Mocked Formatted Message");
    
    // ACT
    logger.info(inputMessage);
    
    // ASSERT
    verify(mockLogger).log(resultLevel.capture(), resultMessage.capture());
    assertEquals(resultLevel.getValue(), Level.INFO);
    assertEquals(resultMessage.getValue(), "Mocked Formatted Message");
  }

  @Test
  @DisplayName("warn - Should call formatter with correct parameters")
  void warnFormatArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setFormatter(mockFormatter);

    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    // ACT
    logger.warn(inputMessage);
    logger.warn(inputMessage, new IOException("hello"));

    // ASSERT
    verify(mockFormatter, atLeastOnce()).format(resultLevel.capture(), resultMessage.capture(), resultMessage.capture(), resultMessage.capture());
    assertEquals(resultMessage.getAllValues().get(0), "br.com.laboratoriodogragas.springlogger.SpringLoggerTest");
    assertEquals(resultMessage.getAllValues().get(1), "warnFormatArgs");
    assertEquals(resultMessage.getAllValues().get(2), inputMessage);
    assertEquals(Level.WARNING, resultLevel.getValue());
  }

  @Test
  @DisplayName("warn - Should call logger with correct parameters")
  void warnLoggingArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    Logger mockLogger = mock(Logger.class);
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setLogger(mockLogger).setFormatter(mockFormatter);
    
    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    when(mockFormatter.format(any(Level.class), anyString(), anyString(), anyString()))
      .thenReturn("Mocked Formatted Message");
    
    // ACT
    logger.warn(inputMessage);
    
    // ASSERT
    verify(mockLogger).log(resultLevel.capture(), resultMessage.capture());
    assertEquals(resultLevel.getValue(), Level.WARNING);
    assertEquals(resultMessage.getValue(), "Mocked Formatted Message");
  }

  @Test
  @DisplayName("error - Should call formatter with correct parameters")
  void errorFormatArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setFormatter(mockFormatter);

    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    // ACT
    logger.error(inputMessage);
    logger.error(inputMessage, new IOException("hello"));

    // ASSERT
    verify(mockFormatter, atLeastOnce()).format(resultLevel.capture(), resultMessage.capture(), resultMessage.capture(), resultMessage.capture());
    assertEquals(resultMessage.getAllValues().get(0), "br.com.laboratoriodogragas.springlogger.SpringLoggerTest");
    assertEquals(resultMessage.getAllValues().get(1), "errorFormatArgs");
    assertEquals(resultMessage.getAllValues().get(2), inputMessage);
    assertEquals(Level.SEVERE, resultLevel.getValue());
  }

  @Test
  @DisplayName("error - Should call logger with correct parameters")
  void errorLoggingArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    Logger mockLogger = mock(Logger.class);
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setLogger(mockLogger).setFormatter(mockFormatter);
    
    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    when(mockFormatter.format(any(Level.class), anyString(), anyString(), anyString()))
      .thenReturn("Mocked Formatted Message");
    
    // ACT
    logger.error(inputMessage);
    
    // ASSERT
    verify(mockLogger).log(resultLevel.capture(), resultMessage.capture());
    assertEquals(resultLevel.getValue(), Level.SEVERE);
    assertEquals(resultMessage.getValue(), "Mocked Formatted Message");
  }

  @Test
  @DisplayName("fatal - Should call formatter with correct parameters")
  void fatalFormatArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setFormatter(mockFormatter);

    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    // ACT
    logger.fatal(inputMessage);
    logger.fatal(inputMessage, new IOException("hello"));

    // ASSERT
    verify(mockFormatter, atLeastOnce()).format(resultLevel.capture(), resultMessage.capture(), resultMessage.capture(), resultMessage.capture());
    assertEquals(resultMessage.getAllValues().get(0), "br.com.laboratoriodogragas.springlogger.SpringLoggerTest");
    assertEquals(resultMessage.getAllValues().get(1), "fatalFormatArgs");
    assertEquals(resultMessage.getAllValues().get(2), inputMessage);
    assertEquals(Level.SEVERE, resultLevel.getValue());
  }

  @Test
  @DisplayName("fatal - Should call logger with correct parameters")
  void fatalLoggingArgs() {
    // ARRANGE
    SpringLogger logger = new SpringLogger();
    String inputMessage = "any_message";
    
    Logger mockLogger = mock(Logger.class);
    FormatLogger mockFormatter = mock(FormatLogger.class);
    logger.setLogger(mockLogger).setFormatter(mockFormatter);
    
    ArgumentCaptor<String> resultMessage = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Level> resultLevel = ArgumentCaptor.forClass(Level.class);

    when(mockFormatter.format(any(Level.class), anyString(), anyString(), anyString()))
      .thenReturn("Mocked Formatted Message");
    
    // ACT
    logger.fatal(inputMessage);
    
    // ASSERT
    verify(mockLogger).log(resultLevel.capture(), resultMessage.capture());
    assertEquals(resultLevel.getValue(), Level.SEVERE);
    assertEquals(resultMessage.getValue(), "Mocked Formatted Message");
  }
}
