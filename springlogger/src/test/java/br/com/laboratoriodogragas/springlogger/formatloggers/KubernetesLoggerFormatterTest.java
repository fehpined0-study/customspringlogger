package br.com.laboratoriodogragas.springlogger.formatloggers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.util.logging.Level;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.laboratoriodogragas.springlogger.dataclasses.BusinessData;
import br.com.laboratoriodogragas.springlogger.dataclasses.LogRecord;
import br.com.laboratoriodogragas.springlogger.interfaces.FormatLogger;

@TestInstance(Lifecycle.PER_CLASS)
public class KubernetesLoggerFormatterTest {
  private Clock mockClock;
  private ObjectMapper mockJSON;

  @BeforeAll
  public void beforeAll() {
    mockClock = mock(Clock.class);
    mockJSON = mock(ObjectMapper.class);

    // 04/06/2024 23:55:40 UTC
    when(mockClock.millis()).thenReturn(1717556140000L);
  }

  @Test
  @DisplayName("Should log with null business data if log content is not instance of LogRecord")
  void logWithNullBusinessData() {
    // ARRANGE
    var input = new Object() {
      Level logLevel = Level.INFO;
      String callerClass = "MockCallerClass";
      String callerMethod = "MockCallerMethod";
      String logContent = "any_log_message";
    };
    String expectedResult = "{\"level\":\"INFO\",\"date\":\"04/06/2024 23:55:40\",\"sourceClass\":\"MockCallerClass\",\"sourceMethod\":\"MockCallerMethod\",\"message\":\"any_log_message\",\"businessData\":null}";

    FormatLogger formatter = new KubernetesLoggerFormatter(mockClock);

    // ACT
    String result = formatter.format(input.logLevel, input.callerClass, input.callerMethod, input.logContent);

    // ASSERT
    assertEquals(expectedResult, result);
  }

  @Test
  @DisplayName("Should log with business data if log content is instance of LogRecord")
  void logWithBusinessData() {
    // ARRANGE
    var input = new Object() {
      Level logLevel = Level.INFO;
      String callerClass = "MockCallerClass";
      String callerMethod = "MockCallerMethod";
      LogRecord logContent = new LogRecord("any_message",
          new BusinessData<Object>(new Object() {
            @JsonProperty
            String dummyData = "any_business_data";
          }));
    };
    String expectedResult = "{\"level\":\"INFO\",\"date\":\"04/06/2024 23:55:40\",\"sourceClass\":\"MockCallerClass\",\"sourceMethod\":\"MockCallerMethod\",\"message\":\"any_message\",\"businessData\":{\"data\":{\"dummyData\":\"any_business_data\"}}}";

    FormatLogger formatter = new KubernetesLoggerFormatter(mockClock);

    // ACT
    String result = formatter.format(input.logLevel, input.callerClass, input.callerMethod, input.logContent);

    // ASSERT
    assertEquals(expectedResult, result);
  }

  @Test
  @DisplayName("Should log with custom business data message if it errors on serialization")
  void logWithErroredBusinessData() throws JsonProcessingException {
    // ARRANGE
    var input = new Object() {
      Level logLevel = Level.INFO;
      String callerClass = "MockCallerClass";
      String callerMethod = "MockCallerMethod";
      LogRecord logContent = new LogRecord("any_message",
          new BusinessData<Object>(new Object() {
            @JsonProperty
            String dummyData = "any_business_data";
          }));
    };
    String expectedResult = "Failed to serialize businessData log object!";

    when(mockJSON.writeValueAsString(any(Object.class))).thenThrow(mock(JsonProcessingException.class));

    FormatLogger formatter = new KubernetesLoggerFormatter(mockClock, mockJSON);

    // ACT
    String result = formatter.format(input.logLevel, input.callerClass, input.callerMethod, input.logContent);

    // ASSERT
    assertEquals(expectedResult, result);
  }
}
