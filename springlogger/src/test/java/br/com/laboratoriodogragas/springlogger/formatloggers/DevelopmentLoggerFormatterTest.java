package br.com.laboratoriodogragas.springlogger.formatloggers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.Clock;
import java.util.logging.Level;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.laboratoriodogragas.springlogger.dataclasses.BusinessData;
import br.com.laboratoriodogragas.springlogger.dataclasses.LogRecord;
import br.com.laboratoriodogragas.springlogger.interfaces.FormatLogger;

@TestInstance(Lifecycle.PER_CLASS)
public class DevelopmentLoggerFormatterTest {
  private Clock mockClock;
  private ObjectMapper mockJSON;

  @BeforeAll
  public void beforeAll() {
    mockClock = mock(Clock.class);
    mockJSON = mock(ObjectMapper.class);

    // 04/06/2024 23:55:40 UTC
    when(mockClock.millis()).thenReturn(1717556140000L);
  }

  @Test
  @DisplayName("Should format using business data if log content is a LogRecord")
  void formatUsingBusinessData() {
    // ARRANGE
    var input = new Object() {
      Level logLevel = Level.INFO;
      String callerClass = "MockCallerClass";
      String callerMethod = "MockCallerMethod";
      LogRecord logContent = new LogRecord("any_message",
          new BusinessData<Object>(new Object() {
            @JsonProperty
            String dummyData = "any_business_data";
          }));
    };
    String expectedResult = "[INFO | 04/06/2024 23:55:40 | MockCallerClass -> MockCallerMethod] - any_message | Business Data: {\"data\":{\"dummyData\":\"any_business_data\"}}";

    FormatLogger formatter = new DevelopmentLoggerFormatter(mockClock);

    // ACT
    String result = formatter.format(input.logLevel, input.callerClass,
        input.callerMethod, input.logContent);

    // ASSERT
    assertEquals(expectedResult, result);
  }

  @Test
  @DisplayName("Should format with custom business data if object serialization throws")
  void formatWhenBusinessDataThrows() throws IOException {
    // ARRANGE
    var input = new Object() {
      Level logLevel = Level.INFO;
      String callerClass = "MockCallerClass";
      String callerMethod = "MockCallerMethod";
      LogRecord logContent = new LogRecord("any_message",
          new BusinessData<Object>(new Object() {
            @JsonProperty
            String dummyData = "any_business_data";
          }));
    };
    String expectedResult = "[INFO | 04/06/2024 23:55:40 | MockCallerClass -> MockCallerMethod] - any_message | Business Data: Tried to serialize, but throwed exception!";

    when(mockJSON.writeValueAsString(any(BusinessData.class))).thenThrow(mock(JsonProcessingException.class));

    FormatLogger formatter = new DevelopmentLoggerFormatter(mockClock, mockJSON);

    // ACT
    String result = formatter.format(input.logLevel, input.callerClass,
        input.callerMethod, input.logContent);

    // ASSERT
    assertEquals(expectedResult, result);
  }

  @Test
  @DisplayName("Should add just the string value of log content if it is not instance of LogRecord")
  void bestEffortFormat() {
    // ARRANGE
    FormatLogger formatter = new DevelopmentLoggerFormatter(mockClock);

    var input = new Object() {
      Level logLevel = Level.INFO;
      String callerClass = "MockCallerClass";
      String callerMethod = "MockCallerMethod";
      String logContent = "any_log_content";
    };
    String expectedResult = "[INFO | 04/06/2024 23:55:40 | MockCallerClass -> MockCallerMethod] - any_log_content | Business Data: none";

    // ACT
    String result = formatter.format(input.logLevel, input.callerClass,
        input.callerMethod, input.logContent);

    // ASSERT
    assertEquals(expectedResult, result);
  }

  @Test
  @DisplayName("Should add ASCII red color to log if it is of level SEVERE")
  void severeLogColoring() {
    // ARRANGE
    FormatLogger formatter = new DevelopmentLoggerFormatter(mockClock);

    var input = new Object() {
      Level logLevel = Level.SEVERE;
      String callerClass = "MockCallerClass";
      String callerMethod = "MockCallerMethod";
      String logContent = "any_log_content";
    };

    // ACT
    String result = formatter.format(input.logLevel, input.callerClass,
        input.callerMethod, input.logContent);

    // ASSERT
    assertTrue(result.contains("\u001B[31m"));
    assertTrue(result.contains("\u001B[0m"));
  }

  @Test
  @DisplayName("Should add ASCII orange color to log if it is of level WARNING")
  void warningLogColoring() {
    // ARRANGE
    FormatLogger formatter = new DevelopmentLoggerFormatter(mockClock);

    var input = new Object() {
      Level logLevel = Level.WARNING;
      String callerClass = "MockCallerClass";
      String callerMethod = "MockCallerMethod";
      String logContent = "any_log_content";
    };

    // ACT
    String result = formatter.format(input.logLevel, input.callerClass,
        input.callerMethod, input.logContent);

    // ASSERT
    assertTrue(result.contains("\u001B[33m"));
    assertTrue(result.contains("\u001B[0m"));
  }
}
